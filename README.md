# SuperSave

Interactive utility for turning DIS/FIX 80 object files into a PROGRAM file. 
Its features include adding the E/A utilities, and detecting start and end addresses for you.
You can convert programs that were designed only to run from E/A 3.

Discussion thread here:

https://forums.atariage.com/topic/287103-supersave-save-ea3-files-as-ea5-files

